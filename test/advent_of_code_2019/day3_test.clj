(ns advent-of-code-2019.day3-test
  (:require [clojure.test :refer :all]
            [clojure.set]
            [advent-of-code-2019.day3 :as day3]))

(deftest parse-tests
  (testing "parsing of columns"
    (is (= {:direction \U :distance 10} (day3/parse-column "U10")))
    (is (= {:direction \R :distance 44} (day3/parse-column "R44")))
    (is (= {:direction \D :distance 33} (day3/parse-column "D33")))
    (is (= {:direction \L :distance 678} (day3/parse-column "L678")))
    )
  (testing "parsing of lines"
    (is (= [{:direction \U :distance 1} {:direction \R :distance 3}]
           (day3/parse-line "U1,R3"))))
  )

(deftest convert-tests
  (testing "convert direction letter to increments for the 2 directions x and y"
    (is (= [0  1] (day3/direction-vector \U)))
    (is (= [0 -1] (day3/direction-vector \D)))
    (is (= [1  0] (day3/direction-vector \R)))
    (is (= [-1 0] (day3/direction-vector \L)))
    )
  (testing "shifting of coordinates"
    (is (= [4 5] (day3/shift-coords [1 2] 3 3)))
    (is (= [-1 3] (day3/shift-coords [1 2] -2 1)))
    )
  (testing "generating wire segment coordinates"
    (is (= [[0 1] [0 2] [0 3] [0 4] [0 5]]
           (day3/generate-segment-coordinates {:direction \U :distance 5} 0 0)))
    (is (= [[3 4] [3 5] [3 6] [3 7] [3 8]]
           (day3/generate-segment-coordinates {:direction \U :distance 5} 3 3)))
    )
  (testing "generating all coordinates of a wire"
    (is (= [[0 1] [0 2] [0 3] [1 3] [2 3] [3 3] [3 2] [3 1] [2 1]]
           (day3/generate-wire-coordinates (day3/parse-line "U3,R3,D2,L1") 0 0 []))))
  )

(deftest solution-tests
  (testing "calculating solution1"
    (is (= day3/sample1-solution
           (day3/calculate-solution1
             (day3/generate-wire-coordinates (first day3/sample1-data) 0 0 [])
             (day3/generate-wire-coordinates (second day3/sample1-data) 0 0 []))))
    (is (= day3/sample2-solution
           (day3/calculate-solution1
             (day3/generate-wire-coordinates (first day3/sample2-data) 0 0 [])
             (day3/generate-wire-coordinates (second day3/sample2-data) 0 0 [])))))

  ;(testing "calculating solution2"
  ;  (is (= day3/sample1-solution
  ;         (day3/calculate-solution2
  ;           (day3/generate-wire-coordinates (first day3/sample1-data) 0 0 [])
  ;           (day3/generate-wire-coordinates (second day3/sample1-data) 0 0 []))))
  ;  (is (= day3/sample2-solution
  ;         (day3/calculate-solution2
  ;           (day3/generate-wire-coordinates (first day3/sample2-data) 0 0 [])
  ;           (day3/generate-wire-coordinates (second day3/sample2-data) 0 0 [])))))
  )