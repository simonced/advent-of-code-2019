(ns advent-of-code-2019.day4)

;; problem input data
(def input-min 125730)
(def input-max 579381)

;; generate list of possible passwords
(def passwords (range input-min (+ 1 input-max)))

;; utilities

(defn split-digits
  "Number is the number we want to split the digits of
123456 -> '(1 2 3 4 5 6)"
  [number]
  (->> (clojure.string/split (str number) #"")
       (map (fn [n] (Integer/parseInt n)))))

(defn adjacent-pairs
  "Nums is a collection of the digits
'(1 2 3 4 5 6) -> [[1 2] [2 3] [3 4] [4 5] [5 6]]"
  [nums]
  (let [take-pair #(take 2 (drop % nums))]
    (map take-pair (range 5))))

;; criterias check

(defn have-two-adjacent-same?
  "Find if 2 adjacent digts are the same in a giver number"
  [number]
  (->> number
       split-digits
       adjacent-pairs
       (some #(= (first %) (second %)))))

(defn have-numbers-increasing?
  "Are all numbers in increasing order?"
  [number]
  (->> number
       split-digits
       adjacent-pairs
       (map #(apply compare %))
       (every? #(<= % 0))))

;; solution to part 1

(def solution1
  (count
    (filter #(and
      (have-two-adjacent-same? %)
      (have-numbers-increasing? %))
    passwords)))

;; solution to part 2

(defn have-one-pair
  "Will group identical numbers and count the lenght of the groups.
  If at list one group is lenght 2, we return true."
  [number]
  (->> number
       split-digits
       (partition-by identity)
       (map count)
       (some #(= 2 %))))

(def solution2
  (count
    (filter #(and
               (have-numbers-increasing? %)
               (have-one-pair %))
            passwords)))