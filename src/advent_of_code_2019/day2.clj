(ns advent-of-code-2019.day2)

(def data [1 0 0 3 1 1 2 3 1 3 4 3 1 5 0 3 2 1 10 19 1 19 5 23 1 6 23 27 1 27 5 31 2 31 10 35 2 35 6 39 1 39 5 43 2 43 9 47 1 47 6 51 1 13 51 55 2 9 55 59 1 59 13 63 1 6 63 67 2 67 10 71 1 9 71 75 2 75 6 79 1 79 5 83 1 83 5 87 2 9 87 91 2 9 91 95 1 95 10 99 1 9 99 103 2 103 6 107 2 9 107 111 1 111 5 115 2 6 115 119 1 5 119 123 1 123 2 127 1 127 9 0 99 2 0 14 0])

(defn set-inputs
  "Sets inpunts in data"
  [data_ i1_ i2_]
  (-> data_ (assoc 1 i1_) (assoc 2 i2_)))

(def data-fixed
  "First we need to fix 2 positions of the program"
  (set-inputs data 12 2))

(defn part1
  "Run the program"
  ([data_] (part1 data_ 0))
  ([data_ position_]
   (if (= 99 (nth data_ position_))
     data_
     (let [operation (nth data_ position_)
           d1        (nth data_ (nth data_ (+ 1 position_)))
           d2        (nth data_ (nth data_ (+ 2 position_)))
           result    (case operation
                       1 (+ d1 d2)
                       2 (* d1 d2))
           result-p  (nth data_ (+ 3 position_))
           next-p    (+ 4 position_)]
    ;    (prn position_ d1 d2 result-p next-p) ; DBG
       (-> data_ (assoc result-p result) (recur next-p)))
   )
  )
)

(def solution1 (-> data-fixed part1 first)) ; sould be 3267740


(def target-value 19690720)

(defn is-solution?
  "Data provided should not contain inputs set"
  [data_ target_ noun_ verb_]
  (= target_ (-> (set-inputs data_ noun_ verb_) part1 first)))

(defn find-solution
  [data_ target_ pairs]
  (let [[n v] (first pairs)
        next (rest pairs)]
    (if
     (is-solution? data_ target_ n v)
      (+ v (* 100 n))
      (cond (seq next) (recur data_ target_ next)))
    )
  )

(defn part2
  "Solution of part 2
enumerates all number combinaisons and return the correct pair"
  [data_]
  (let [nouns (range 100)
        verbs (range 100)
        pairs (for [n nouns v verbs] [n v])]
    (find-solution data_ target-value pairs)))

(def solution2 (part2 data))