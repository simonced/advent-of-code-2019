(ns advent-of-code-2019.day3
  (:require [clojure.string :as str]))

(defn read-input
  "Simply read and return the content of the input text file"
  []
  (clojure.core/slurp "resources/input_day3.txt"))

;;; INPUT ANALYSIS/PARSING

(defn parse-column
  "Parse a column and return a struct with direction and distance"
  [column_]
  (let [dire (-> column_ first)
        dist (-> column_ rest str/join read-string)]
    {:direction dire :distance dist}))

(defn parse-line
  "Parse a single line"
  [line_]
  (let [cols (str/split line_ #",")]
    (map parse-column cols)))

(defn parse-input
  "We simply parse the coordinates of the 2 wires in the text file
and make it so it'll be easy to work with"
  [in_]
  (let [lines (str/split-lines in_)]
    (map parse-line lines)))

(def data (parse-input (read-input)))

;;; some sample data for testing
(def sample1-data (parse-input "R75,D30,R83,U83,L12,D49,R71,U7,L72\r\nU62,R66,U55,R34,D71,R55,D58,R83"))
(def sample1-solution 159)
(def sample1-solution2 610)

(def sample2-data (parse-input "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\r\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7"))
(def sample2-solution 135)
(def sample2-solution2 410)

;;; DATA CONVERSION/GENERATION

(defn direction-vector
  "simply matches one of the 4 letters of direction.
Returns [x y] where x and y are increments of each wire 'part of segment'."
  [direction_]
  (case direction_
    \U [0 1]
    \D [0 -1]
    \R [1 0]
    \L [-1 0]))

(defn shift-coords
  "Shift coordinates to a different origin"
  [coords_ x_ y_]
  (let [[x y] coords_]
    [(+ x x_) (+ y y_)]))

(defn generate-segment-coordinates
  "Generates wire segment coordinates from an origin at x_ y_.
seg_ is the struct returned by parse-column"
  [seg_ x_ y_]
  (let [{direc :direction dist :distance} seg_
        vec (direction-vector direc)
        f (fn [p] (map #(* p %) vec)) ; multiply each vector direction to p
        parts (for [n (filter #(> % 0) (range (+ 1 dist)))] n) ; can it be simplified?
        parts-coord (map f parts)]
    (map #(shift-coords % x_ y_) parts-coord)))

(defn generate-wire-coordinates
  "Brutal approach! generating all the coordinates of a wire"
  [wire_ x_ y_ acc_]
  (let [segment (first wire_)
        next (rest wire_)
        new-coords (generate-segment-coordinates segment x_ y_)
        all (apply merge acc_ new-coords)]
    (if (seq next)
      (recur next (-> new-coords last first) (-> new-coords last last) all)
      all)))

;;; careful! heavy computation
(def wire1-coords (generate-wire-coordinates (first data) 0 0 []))
(def wire2-coords (generate-wire-coordinates (second data) 0 0 []))

;;; === PART 1 ===

(defn calculate-solution1
  "Calculates solution for part 1"
  [wire1 wire2]
  (->> (clojure.set/intersection
         (into #{} wire1)
         (into #{} wire2))
       (map #(+ (-> % first Math/abs) (-> % second Math/abs)))
       (sort-by #(Math/abs %))
       first))

(def solution1 (calculate-solution1 wire1-coords wire2-coords)) ; it's 209!

;;; === PART 2 ===